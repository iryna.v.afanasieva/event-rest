package org.learn.module.eight.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.function.BiConsumer;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.learn.module.eight.configuration.DestinationProperties;
import org.learn.module.eight.dto.Event;
import org.springframework.stereotype.Component;

/**
 * Producer Service.
 */
@Slf4j
@Component
@AllArgsConstructor
public class ProducerService {

  private final ObjectMapper mapper;
  private final DestinationProperties properties;

  public void createEventNotification(final Event event,
                                      final BiConsumer<String, String> consumer) {
    sendMessage(properties.getNotification().getCreate(), event, consumer);
  }

  public void updateEventNotification(final Event event,
                                      final BiConsumer<String, String> consumer) {
    sendMessage(properties.getNotification().getUpdate(), event, consumer);
  }

  public void deleteEventNotification(final Long id,
                                      final BiConsumer<String, String> consumer) {
    sendMessage(properties.getNotification().getUpdate(), id, consumer);
  }

  private <T> void sendMessage(final String destination, final T event,
                               final BiConsumer<String, String> consumer) {
    try {
      consumer.accept(destination, mapper.writeValueAsString(event));
    } catch (JsonProcessingException e) {
      log.error(String.format("Unable to send message into %s topic", destination), e);
    }
  }

}
