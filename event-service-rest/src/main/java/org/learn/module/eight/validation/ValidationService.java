package org.learn.module.eight.validation;

import org.learn.module.eight.dto.Event;

/**
 * Validation Service.
 */
public interface ValidationService {
  void validate(Event event);
}
