package org.learn.module.eight.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotNull;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;
import org.learn.module.eight.enumeration.EventType;

/**
 * Event.
 */
@Data
@NotNull
@Builder(toBuilder = true)
@AllArgsConstructor
@Jacksonized
@NoArgsConstructor
public class Event {

  @NotNull
  private Long eventId;

  @NotNull
  private String title;

  @NotNull
  private String place;

  @NotNull
  private String speaker;

  @NotNull
  private EventType eventType;

  @NotNull
  @Future
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  private LocalDateTime dateTime;
}
