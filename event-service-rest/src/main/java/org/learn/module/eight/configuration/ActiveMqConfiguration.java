package org.learn.module.eight.configuration;

import jakarta.jms.Queue;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;

/**
 * Active MQ Configuration.
 */
@Configuration
@EnableJms
@Profile("activemq")
public class ActiveMqConfiguration {

  @Value("${spring.activemq.broker-url}")
  private String brokerUrl;

  @Bean
  public Queue createEventRequest() {
    return new ActiveMQQueue("create-event-request");
  }

  @Bean
  public Queue updateEventRequest() {
    return new ActiveMQQueue("update-event-request");
  }

  @Bean
  public Queue deleteEventRequest() {
    return new ActiveMQQueue("delete-event-request");
  }

  /**
   * Active MQ Connection Factory.
   *
   * @return Active MQ Connection Factory.
   */
  @Bean
  public ActiveMQConnectionFactory activeMqConnectionFactory() {
    ActiveMQConnectionFactory activeMqConnectionFactory = new ActiveMQConnectionFactory();
    activeMqConnectionFactory.setBrokerURL(brokerUrl);
    return activeMqConnectionFactory;
  }

  @Bean
  public JmsTemplate jmsTemplate() {
    return new JmsTemplate(activeMqConnectionFactory());
  }

}
