package org.learn.module.eight.kafka;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.service.EventMessaging;
import org.learn.module.eight.service.ProducerService;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * Kafka Event Producer.
 */
@Slf4j
@Component
@Profile("kafka")
@AllArgsConstructor
public class EventProducer implements EventMessaging {

  private final KafkaTemplate<String, String> kafkaTemplate;
  private final ProducerService producerService;

  @Override
  public void createEvent(final Event event) {
    producerService.createEventNotification(event, kafkaTemplate::send);
  }

  @Override
  public void updateEvent(final Event event) {
    producerService.updateEventNotification(event, kafkaTemplate::send);
  }

  @Override
  public void deleteEvent(final Long id) {
    producerService.deleteEventNotification(id, kafkaTemplate::send);
  }

}
