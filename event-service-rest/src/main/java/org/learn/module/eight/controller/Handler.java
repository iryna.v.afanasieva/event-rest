package org.learn.module.eight.controller;

import jakarta.validation.ValidationException;
import org.learn.module.eight.exception.EventNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;

/**
 * Controller Advice.
 */
@ControllerAdvice
public class Handler {

  /**
   * Exception Handler.
   *
   * @param exception to handle.
   * @return Response Entity.
   */
  @ExceptionHandler({
      ValidationException.class,
      IllegalArgumentException.class,
      EventNotFoundException.class
  })
  public ResponseEntity<Problem> clientHandler(RuntimeException exception) {
    return new ResponseEntity<>(Problem.builder()
        .withStatus(Status.BAD_REQUEST)
        .withDetail(exception.getMessage())
        .build(), null, HttpStatus.BAD_REQUEST);
  }
}
