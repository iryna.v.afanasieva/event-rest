package org.learn.module.eight.configuration;

import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Declarables;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.EnableJms;

/**
 * Rabbit MQ Configuration.
 */
@Configuration
@EnableJms
@Profile("rabbitmq")
public class RabbitMqConfiguration {
  @Autowired
  private DestinationProperties properties;

  @Bean
  public Declarables createEventRequest() {
    final var topicName = properties.getRequest().getCreate();
    return createDeclarables(topicName);
  }

  @Bean
  public Declarables updateEventRequest() {
    final var topicName = properties.getRequest().getUpdate();
    return createDeclarables(topicName);
  }

  @Bean
  public Declarables deleteEventRequest() {
    final var topicName = properties.getRequest().getDelete();
    return createDeclarables(topicName);
  }

  private Declarables createDeclarables(final String topicName) {
    Queue topicQueue1 = new Queue(topicName + "_queue", false);

    TopicExchange topicExchange = new TopicExchange(topicName);

    return new Declarables(
        topicQueue1,
        topicExchange,
        BindingBuilder
            .bind(topicQueue1)
            .to(topicExchange).with("*.important.*"));
  }
}
