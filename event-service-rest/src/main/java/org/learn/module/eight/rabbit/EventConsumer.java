package org.learn.module.eight.rabbit;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.learn.module.eight.service.ConsumerService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Kafka Event Consumer.
 */
@Slf4j
@Component
@Profile("rabbit")
@AllArgsConstructor
public class EventConsumer {

  private final ConsumerService consumerService;

  /**
   * Create Event.
   *
   * @param message to create.
   */
  @RabbitListener(queues = "${destination.request.create}")
  public void createEvent(final String message) {
    consumerService.createEvent(message);
  }

  /**
   * Update Event.
   *
   * @param message to update.
   */
  @RabbitListener(queues = "${destination.request.update}")
  public void updateEvent(final String message) {
    consumerService.updateEvent(message);
  }

  /**
   * Delete Event.
   *
   * @param identifier to delete.
   */
  @RabbitListener(queues = "${destination.request.delete}")
  public void deleteEvent(final String identifier) {
    consumerService.deleteEvent(identifier);
  }

}
