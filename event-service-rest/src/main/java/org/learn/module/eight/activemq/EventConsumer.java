package org.learn.module.eight.activemq;

import jakarta.jms.JMSException;
import java.util.Optional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.activemq.command.TransactionId;
import org.apache.commons.lang3.StringUtils;
import org.learn.module.eight.service.ConsumerService;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * Kafka Event Consumer.
 */
@Slf4j
@Component
@Profile("activemq")
@AllArgsConstructor
public class EventConsumer {

  private final ConsumerService consumerService;

  /**
   * Create Event.
   *
   * @param message to create.
   */
  @JmsListener(destination = "${destination.request.create}")
  public void createEvent(final ActiveMQTextMessage message) {
    consumerService.createEvent(getStringMessage(message));
  }

  /**
   * Update Event.
   *
   * @param message to update.
   */
  @JmsListener(destination = "${destination.request.update}")
  public void updateEvent(final ActiveMQTextMessage message) {
    consumerService.updateEvent(getStringMessage(message));
  }

  /**
   * Delete Event.
   *
   * @param identifier to delete.
   */
  @JmsListener(destination = "${destination.request.delete}")
  public void deleteEvent(final ActiveMQTextMessage identifier) {
    consumerService.deleteEvent(getStringMessage(identifier));
  }

  private String getStringMessage(final ActiveMQTextMessage message) {
    if (message == null) {
      return StringUtils.EMPTY;
    }

    final String key = String.join("_",
        String.valueOf(message.getTimestamp()),
        Optional.ofNullable(message.getTransactionId())
            .map(TransactionId::getTransactionKey)
            .orElse("noTransactionId"));

    String stringMessage;
    try {
      stringMessage = message.getText();
    } catch (JMSException e) {
      log.error(String.format("Unable to read message for the key: %s.", key), e);
      return StringUtils.EMPTY;
    }

    log.info("Received Key: {}, Message: {}", key, stringMessage);
    return stringMessage;
  }

}
