package org.learn.module.eight.service;

import java.util.List;
import org.learn.module.eight.dto.Event;

/**
 * Event Service.
 */
public interface EventService {
  Event createEvent(final Event event);

  Event updateEvent(final Long id, final Event event);

  Event getEvent(final Long id);

  Event deleteEvent(final Long id);

  List<Event> getAllEvents();
}
