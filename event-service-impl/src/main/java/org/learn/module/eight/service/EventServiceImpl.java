package org.learn.module.eight.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;
import lombok.AllArgsConstructor;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.exception.EventNotFoundException;

/**
 * Event Service Implementation.
 */
@AllArgsConstructor
public class EventServiceImpl implements EventService {

  private final EventMessaging eventMessaging;
  private static final Map<Long, Event> events = new ConcurrentHashMap<>();
  private static final Object lock = new Object();

  @Override
  public Event createEvent(final Event event) {
    synchronized (lock) {
      final var eventId = event.getEventId();
      if (events.containsKey(eventId)) {
        throw new IllegalArgumentException(
            String.format("Event with id %s already exists", eventId));
      }
      save(event);
      if (eventMessaging != null) {
        eventMessaging.createEvent(event);
      }
      return event;
    }
  }

  @Override
  public Event updateEvent(final Long id, final Event event) {
    synchronized (lock) {
      checkExisting(id);
      save(event);
      if (eventMessaging != null) {
        eventMessaging.updateEvent(event);
      }
      return event;
    }
  }

  @Override
  public Event getEvent(final Long id) {
    synchronized (lock) {
      checkExisting(id);
      return events.get(id);
    }
  }

  @Override
  public Event deleteEvent(final Long id) {
    synchronized (lock) {
      checkExisting(id);
      if (eventMessaging != null) {
        eventMessaging.deleteEvent(id);
      }
      return events.remove(id);
    }
  }

  @Override
  public List<Event> getAllEvents() {
    synchronized (lock) {
      return events.values().stream().toList();
    }
  }

  private static void checkExisting(final Long id) {
    synchronized (lock) {
      if (!events.containsKey(id)) {
        throw new EventNotFoundException(String.format("Event not found for id: %s.", id));
      }
    }
  }

  private static void save(final Event event) {
    synchronized (lock) {
      final var key = event.getEventId();
      events.put(key, event);
    }
  }

}
