package org.learn.module.eight.rabbit;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.service.EventMessaging;
import org.learn.module.eight.service.ProducerService;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * Kafka Event Producer.
 */
@Slf4j
@Component
@Profile("rabbit")
@AllArgsConstructor
public class EventProducer implements EventMessaging {

  private final RabbitTemplate rabbitTemplate;

  private final ProducerService producerService;

  @Override
  public void createEvent(final Event event) {
    producerService.createEventNotification(event, rabbitTemplate::convertAndSend);
  }

  @Override
  public void updateEvent(final Event event) {
    producerService.updateEventNotification(event, rabbitTemplate::convertAndSend);
  }

  @Override
  public void deleteEvent(final Long id) {
    producerService.deleteEventNotification(id, rabbitTemplate::convertAndSend);
  }

}
