package org.learn.module.eight.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.enumeration.EventType;
import org.learn.module.eight.exception.EventNotFoundException;

class EventServiceImplTest {

  private static final EventServiceImpl eventService = new EventServiceImpl(null);
  private static final Event EVENT = Event.builder()
      .eventId(1L)
      .eventType(EventType.WORKSHOP)
      .dateTime(LocalDateTime.now().plusDays(1))
      .place("place")
      .speaker("speaker")
      .title("title")
      .build();

  private static final Event UPDATED_EVENT = EVENT.toBuilder()
      .dateTime(LocalDateTime.now().plusDays(2))
      .eventType(EventType.TECH_TALK)
      .build();

  private static final Event OTHER_EVENT = EVENT.toBuilder()
      .eventId(3L)
      .build();

  @BeforeEach
  void setup() {
    final var events = getActualEvents();
    events.forEach(event -> eventService.deleteEvent(event.getEventId()));
  }

  @Test
  void shouldCreateEvent() {
    final var actual = eventService.createEvent(EVENT);

    assertEquals(EVENT, actual);

    assertThat(getActualEvents()).contains(EVENT);
  }

  @Test
  void shouldUpdateEvent() {

    eventService.createEvent(EVENT);

    var actualEvents = getActualEvents();

    assertThat(actualEvents)
        .contains(EVENT);
    assertThat(actualEvents)
        .doesNotContain(UPDATED_EVENT);

    final var actual = eventService.updateEvent(1L, UPDATED_EVENT);

    assertNotEquals(EVENT, actual);
    assertEquals(UPDATED_EVENT, actual);

    actualEvents = getActualEvents();

    assertThat(actualEvents)
        .contains(UPDATED_EVENT);
    assertThat(actualEvents)
        .doesNotContain(EVENT);
  }

  @Test
  void shouldGetEvent() {

    eventService.createEvent(EVENT);

    assertThat(getActualEvents())
        .contains(EVENT);

    final var actual = eventService.getEvent(1L);

    assertEquals(EVENT, actual);

    assertThat(getActualEvents())
        .contains(EVENT);
  }

  @Test
  void shouldGetAllEvents() {

    eventService.createEvent(EVENT);
    eventService.createEvent(OTHER_EVENT);

    assertThat(getActualEvents())
        .containsExactlyInAnyOrder(EVENT, OTHER_EVENT);

    final var actual = eventService.getAllEvents();

    assertThat(actual)
        .containsExactlyInAnyOrder(EVENT, OTHER_EVENT);
  }

  @Test
  void shouldDeleteEvent() {

    eventService.createEvent(EVENT);

    assertThat(getActualEvents())
        .contains(EVENT);

    final var actual = eventService.deleteEvent(1L);
    assertEquals(EVENT, actual);

    assertThat(getActualEvents()).doesNotContain(EVENT);

  }

  @Test
  void shouldThrowEventNotFoundExceptionIfEventIdToDeleteNotPresented() {

    eventService.createEvent(EVENT);

    assertThat(getActualEvents())
        .contains(EVENT);

    final var actual = assertThrows(EventNotFoundException.class,
        () -> eventService.deleteEvent(2L));

    assertEquals("Event not found for id: 2.", actual.getMessage());

    assertThat(getActualEvents()).contains(EVENT);

  }

  private static List<Event> getActualEvents() {
    try {
      final var clazz = eventService.getClass();
      final var events = clazz.getDeclaredField("events");
      events.setAccessible(true);

      return ((Map<Long, Event>) events.get(eventService)).values().stream().toList();
    } catch (Throwable throwable) {
      return List.of();
    }
  }

}