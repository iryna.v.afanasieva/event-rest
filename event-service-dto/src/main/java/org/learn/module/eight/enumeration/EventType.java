package org.learn.module.eight.enumeration;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;

/**
 * Event Type.
 */
public enum EventType {
  WORKSHOP,
  TECH_TALK,

  @JsonEnumDefaultValue
  UNKNOWN
}
