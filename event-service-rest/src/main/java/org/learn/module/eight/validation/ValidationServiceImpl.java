package org.learn.module.eight.validation;

import jakarta.validation.ValidationException;
import jakarta.validation.Validator;
import java.util.Objects;
import lombok.AllArgsConstructor;
import org.learn.module.eight.dto.Event;
import org.springframework.stereotype.Component;

/**
 * Validation Service Implementation.
 */
@Component
@AllArgsConstructor
public class ValidationServiceImpl implements ValidationService {

  private final Validator validator;

  @Override
  public void validate(Event event) {
    final var violations = validator.validate(event);
    if (!violations.isEmpty()) {
      final var message =
          violations.stream()
              .filter(Objects::nonNull)
              .map(violation ->
                  String.join(": ", violation.getPropertyPath().toString(), violation.getMessage()))
              .reduce((x, y) -> String.join(", ", x, y)).orElse("");
      throw new ValidationException(message);
    }
  }
}
