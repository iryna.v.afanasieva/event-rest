package org.learn.module.eight.exception;

/**
 * Event Not Found Exception.
 */
public class EventNotFoundException extends RuntimeException {
  public EventNotFoundException(final String message) {
    super(message);
  }
}
