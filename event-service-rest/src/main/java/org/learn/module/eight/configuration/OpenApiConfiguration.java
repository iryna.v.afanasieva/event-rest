package org.learn.module.eight.configuration;

import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.customizers.GlobalOpenApiCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Open Api Configuration.
 */
@Configuration
public class OpenApiConfiguration {

  /**
   * Api Customizer.
   *
   * @return GlobalOpenApiCustomizer.
   */
  @Bean
  public GlobalOpenApiCustomizer apiCustomizer() {
    return api -> api.info(
        new Info()
            .title("Service - REST API")
            .description("API Spec")
            .version("1")
    );
  }
}
