package org.learn.module.eight.activemq;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.service.EventMessaging;
import org.learn.module.eight.service.ProducerService;
import org.springframework.context.annotation.Profile;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

/**
 * Kafka Event Producer.
 */
@Slf4j
@Component
@Profile("activemq")
@AllArgsConstructor
public class EventProducer implements EventMessaging {

  private final JmsTemplate jmsTemplate;

  private final ProducerService producerService;

  @Override
  public void createEvent(final Event event) {
    producerService.createEventNotification(event, jmsTemplate::convertAndSend);
  }

  @Override
  public void updateEvent(final Event event) {
    producerService.updateEventNotification(event, jmsTemplate::convertAndSend);
  }

  @Override
  public void deleteEvent(final Long id) {
    producerService.deleteEventNotification(id, jmsTemplate::convertAndSend);
  }

}
