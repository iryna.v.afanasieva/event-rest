package org.learn.module.eight.kafka;

import lombok.AllArgsConstructor;
import org.learn.module.eight.service.ConsumerService;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/**
 * Kafka Event Consumer.
 */
@Component
@Profile("kafka")
@AllArgsConstructor
public class EventConsumer {

  private final ConsumerService consumerService;

  /**
   * Create Event.
   *
   * @param message to create.
   */
  @KafkaListener(topics = "${destination.request.create}")
  public void createEvent(final String message) {
    consumerService.createEvent(message);
  }

  /**
   * Update Event.
   *
   * @param message to update.
   */
  @KafkaListener(topics = "${destination.request.update}")
  public void updateEvent(final String message) {
    consumerService.updateEvent(message);
  }

  /**
   * Delete Event.
   *
   * @param identifier to delete.
   */
  @KafkaListener(topics = "${destination.request.delete}")
  public void deleteEvent(final String identifier) {
    consumerService.deleteEvent(identifier);
  }

}
