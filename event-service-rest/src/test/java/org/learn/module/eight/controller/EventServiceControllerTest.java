package org.learn.module.eight.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.enumeration.EventType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("kafka")
@ExtendWith(SpringExtension.class)
@EmbeddedKafka(
    partitions = 1,
    brokerProperties = { "listeners=PLAINTEXT://localhost:9092", "port=9092" }
)
class EventServiceControllerTest {
  private static final String URI = "http://localhost:%d/api/v1/events%s";
  private static final LocalDateTime FUTURE_DATE = LocalDateTime.now().plusDays(1);
  private static final Event EVENT = Event.builder()
      .eventId(1L)
      .eventType(EventType.WORKSHOP)
      .dateTime(
          LocalDateTime.of(
              FUTURE_DATE.getYear(),
              FUTURE_DATE.getMonth(),
              FUTURE_DATE.getDayOfMonth(),
              FUTURE_DATE.getHour(),
              FUTURE_DATE.getMinute()))
      .place("place")
      .speaker("speaker")
      .title("title")
      .build();

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;

  @BeforeEach
  void setup() {
    ResponseEntity<List<Event>> response = restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.GET,
        new HttpEntity<>(EVENT),
        new ParameterizedTypeReference<>() {}
    );
    Objects.requireNonNull(response.getBody()).stream()
        .filter(Objects::nonNull)
        .map(Event::getEventId)
        .toList().forEach(item -> {
          restTemplate.exchange(
              String.format(URI, port, "/" + item),
              HttpMethod.DELETE,
              new HttpEntity<>(null),
              new ParameterizedTypeReference<>() {}
          );
        });

  }

  @Test
  void shouldCreateEvent() {
    ResponseEntity<Event> response = restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.POST,
        new HttpEntity<>(EVENT),
        new ParameterizedTypeReference<>() {}
    );

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(EVENT);
  }

  @Test
  void shouldGetEvent() {
    final var event = EVENT.toBuilder().eventId(10L).build();
    restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.POST,
        new HttpEntity<>(event),
        new ParameterizedTypeReference<>() {}
    );

    ResponseEntity<Event> response = restTemplate.exchange(
        String.format(URI, port, "/10"),
        HttpMethod.GET,
        new HttpEntity<>(EVENT),
        new ParameterizedTypeReference<>() {}
    );

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(event);
  }

  @Test
  void shouldGetAllEvents() {
    restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.POST,
        new HttpEntity<>(EVENT.toBuilder().eventId(11L).build()),
        new ParameterizedTypeReference<>() {}
    );
    restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.POST,
        new HttpEntity<>(EVENT.toBuilder().eventId(12L).build()),
        new ParameterizedTypeReference<>() {}
    );

    ResponseEntity<List<Event>> response = restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.GET,
        new HttpEntity<>(EVENT),
        new ParameterizedTypeReference<>() {}
    );

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody())
        .isEqualTo(List.of(
            EVENT.toBuilder().eventId(11L).build(),
            EVENT.toBuilder().eventId(12L).build()));
  }

  @Test
  void shouldUpdateEvent() {
    final var event = EVENT.toBuilder().eventId(2L).build();
    restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.POST,
        new HttpEntity<>(event),
        new ParameterizedTypeReference<>() {}
    );

    ResponseEntity<Event> response = restTemplate.exchange(
        String.format(URI, port, "/2"),
        HttpMethod.PUT,
        new HttpEntity<>(event),
        new ParameterizedTypeReference<>() {}
    );

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(event);
  }

  @Test
  void shouldDeleteEvent() {
    final var event = EVENT.toBuilder().eventId(3L).build();

    restTemplate.exchange(
        String.format(URI, port, ""),
        HttpMethod.POST,
        new HttpEntity<>(event),
        new ParameterizedTypeReference<>() {}
    );

    ResponseEntity<Event> response = restTemplate.exchange(
        String.format(URI, port, "/3"),
        HttpMethod.DELETE,
        new HttpEntity<>(null),
        new ParameterizedTypeReference<>() {}
    );

    assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(response.getBody()).isEqualTo(event);
  }

}
