package org.learn.module.eight.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ValidationException;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.validation.ValidationService;
import org.springframework.stereotype.Component;

/**
 * Consumer Service.
 */
@Slf4j
@Component
@AllArgsConstructor
public class ConsumerService {

  private ObjectMapper mapper;
  private ValidationService validationService;
  private EventService eventService;

  /**
   * Create Event.
   *
   * @param message to create.
   */
  public void createEvent(final String message) {
    getEventStream(message)
        .forEach(event -> {
          try {
            validationService.validate(event);
            eventService.createEvent(event);
          } catch (ValidationException exception) {
            log.error("ValidationException:", exception);
          }
        });
  }

  /**
   * Update event.
   *
   * @param message to update.
   */
  public void updateEvent(final String message) {
    getEventStream(message)
        .forEach(event -> {
          try {
            validationService.validate(event);
            eventService.updateEvent(event.getEventId(), event);
          } catch (ValidationException exception) {
            log.error("ValidationException:", exception);
          }
        });
  }

  /**
   * Delete Event.
   *
   * @param identifier to delete.
   */
  public void deleteEvent(final String identifier) {
    Optional.ofNullable(identifier)
        .stream()
        .filter(Objects::nonNull)
        .filter(StringUtils::isNumeric)
        .map(Long::valueOf)
        .forEach(eventService::deleteEvent);
  }

  private Stream<Event> getEventStream(final String message) {
    return Optional.ofNullable(message)
        .stream()
        .filter(Objects::nonNull)
        .map(item -> {
          try {
            return mapper.readValue(item, Event.class);
          } catch (JsonProcessingException e) {
            log.error(String.format("Unable to deserialize %s", item), e);
            return null;
          }
        })
        .filter(Objects::nonNull);
  }

}
