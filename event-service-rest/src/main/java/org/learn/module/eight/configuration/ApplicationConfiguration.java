package org.learn.module.eight.configuration;

import org.learn.module.eight.service.EventMessaging;
import org.learn.module.eight.service.EventService;
import org.learn.module.eight.service.EventServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Application Configuration.
 */
@Configuration
public class ApplicationConfiguration {

  @Bean
  public EventService eventService(EventMessaging eventMessaging) {
    return new EventServiceImpl(eventMessaging);
  }

}
