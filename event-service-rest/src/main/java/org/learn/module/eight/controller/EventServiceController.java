package org.learn.module.eight.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.learn.module.eight.dto.Event;
import org.learn.module.eight.service.EventService;
import org.learn.module.eight.validation.ValidationService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Event Service Controller.
 */
@Slf4j
@RestController
@RequestMapping("api/v1/events")
@RequiredArgsConstructor
public class EventServiceController {

  private final EventService eventService;
  private final ValidationService validationService;

  /**
   * Create Event.
   *
   * @param event to create
   * @return created event.
   */
  @PostMapping
  @ApiResponse(
      content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = Event.class)))
  @Operation(description = "Create Event Data.", summary = "Create Event.")
  public Event createEvent(@RequestBody Event event) {
    validationService.validate(event);
    return eventService.createEvent(event);
  }

  /**
   * Update Event.
   *
   * @param id to update
   * @param event to update
   * @return updated event.
   */
  @PutMapping("/{id}")
  @ApiResponse(
      content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = Event.class)))
  @Operation(description = "Update Event Data.", summary = "Update Event.")
  public Event updateEvent(@PathVariable long id, @RequestBody Event event) {
    validationService.validate(event);
    if (!event.getEventId().equals(id)) {
      throw new IllegalArgumentException("Id mismatch detected.");
    }

    return eventService.updateEvent(id, event);
  }

  /**
   * Get Event.
   */
  @GetMapping("/{id}")
  @ApiResponse(
      content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = Event.class)))
  @Operation(description = "Get Event by Id.", summary = "Get Event.")
  public Event getEvent(@PathVariable long id) {
    return eventService.getEvent(id);
  }

  /**
   * Get All Events.
   */
  @GetMapping
  @ApiResponse(
      content = @Content(mediaType = "application/json",
          array = @ArraySchema(schema = @Schema(implementation = Event.class))))
  @Operation(description = "Get All Events.", summary = "Get All Events.")
  public List<Event> getEvents() {
    return eventService.getAllEvents();
  }

  /**
   * Delete Events.
   */
  @DeleteMapping("/{id}")
  @ApiResponse(
      content = @Content(mediaType = "application/json",
          schema = @Schema(implementation = Event.class)))
  @Operation(description = "Delete Event Data.", summary = "Delete Event.")
  public Event deleteEvent(@PathVariable long id) {
    return eventService.deleteEvent(id);
  }

}
