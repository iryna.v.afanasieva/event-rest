package org.learn.module.eight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot Application.
 */
@SpringBootApplication
public class Application {

  /**
   * Main SpringBoot application start.
   *
   * @param args parameters
   */
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

}
