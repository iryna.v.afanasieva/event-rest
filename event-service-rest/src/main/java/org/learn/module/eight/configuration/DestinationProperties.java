package org.learn.module.eight.configuration;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Kafka Properties Configuration.
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "destination")
@NoArgsConstructor
@AllArgsConstructor
public class DestinationProperties {

  private Notification notification;
  private Request request;

  /**
   * Notification configuration.
   */
  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Notification {
    private String create;
    private String update;
    private String delete;
  }

  /**
   * Request configuration.
   */
  @Data
  @NoArgsConstructor
  @AllArgsConstructor
  public static class Request {
    private String create;
    private String update;
    private String delete;
  }
}
