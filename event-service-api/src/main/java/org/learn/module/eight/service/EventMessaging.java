package org.learn.module.eight.service;

import org.learn.module.eight.dto.Event;

/**
 * Event Messaging Interface.
 */
public interface EventMessaging {
  void createEvent(final Event event);

  void updateEvent(final Event event);

  void deleteEvent(final Long id);
}
