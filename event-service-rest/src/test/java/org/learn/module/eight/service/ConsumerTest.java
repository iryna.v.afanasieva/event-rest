package org.learn.module.eight.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.learn.module.eight.validation.ValidationService;

class ConsumerTest {

  private static final ObjectMapper mapper =
      new ObjectMapper().registerModule(new JavaTimeModule());

  private static final String EVENT =
      """
        {
          "eventId":1,
          "title":"title",
          "place":"place",
          "speaker":"speaker",
          "eventType":"WORKSHOP",
          "dateTime":"2023-09-03T10:00:00.000Z"
        }
      """;
  
  private ValidationService validationService;
  private EventService eventService;

  private ConsumerService consumerService;

  @BeforeEach
  void init() {
    validationService = mock(ValidationService.class);
    eventService = mock(EventService.class);
    consumerService = new ConsumerService(mapper, validationService, eventService);
  }

  @Test
  void shouldCreateEvent() {
    consumerService.createEvent(EVENT);

    verify(validationService).validate(any());
    verify(eventService).createEvent(any());
  }

  @Test
  void shouldUpdateEvent() {
    consumerService.updateEvent(EVENT);

    verify(validationService).validate(any());
    verify(eventService).updateEvent(eq(1L), any());
  }

  @Test
  void shouldDeleteEvent() {
    consumerService.deleteEvent("1");

    verifyNoInteractions(validationService);
    verify(eventService).deleteEvent(1L);
  }

}
